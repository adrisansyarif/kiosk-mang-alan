<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     // return view('welcome');
//     $viewData = [];
//     $viewData['title'] = "Laravel - Online";
//     return view('home.index')->with('viewData', $viewData);
// });

Route::get('/', 'App\http\Controllers\HomeController@index')->name('home.index');
Route::get('/about', 'App\http\Controllers\HomeController@about')->name('home.about');
Route::get('/product', 'App\http\Controllers\ProductController@index')->name('product.index');
Route::get('/product/{id}', 'App\http\Controllers\ProductController@show')->name('product.show');

Route::get('/cart', 'App\Http\Controllers\CartController@index')->name("cart.index");
Route::get('/cart/delete', 'App\Http\Controllers\CartController@delete')->name("cart.delete");
Route::post('/cart/add/{id}', 'App\Http\Controllers\CartController@add')->name("cart.add");

Route::middleware('auth')->group(function() {
    Route::get('/cart/purcase', 'App\Http\Controllers\CartController@purchase')->name('cart.purchase');
    Route::get('/my-account/orders', 'App\Http\Controllers\MyAccountController@orders')->name("myaccount.orders");

});

Route::middleware('admin')->group(function() {

    Route::get('/admin', 'App\http\Controllers\Admin\AdminHomeController@index')->name('admin.home.index');
    Route::get('/admin/products', 'App\http\Controllers\Admin\AdminProductController@index')->name('admin.product.index');
    Route::post('/admin/products/strore', 'App\http\Controllers\Admin\AdminProductController@store')->name('admin.product.store');
    Route::delete('/admin/products/{id}/delete', 'App\http\Controllers\Admin\AdminProductController@delete')->name('admin.product.delete');
    Route::get('/admin/products/{id}/edit', 'App\http\Controllers\Admin\AdminProductController@edit')->name('admin.product.edit');
    Route::put('/admin/products/{id}/update', 'App\http\Controllers\Admin\AdminProductController@update')->name('admin.product.update');
});

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Storage;
use App\Models\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminProductController extends Controller
{
    public function index()
    {
        $viewData = [];
        $viewData["title"] = "Admin Page - Products - Kiosk Mang Alan";
        $viewData["products"] = Product::all();
        return view('admin.product.index')->with("viewData", $viewData);
    }

    public function store(Request $request)
    {
        Product::validate($request);
        $newProduct = new Product();
        $newProduct->setName($request->input('name'));
        $newProduct->setDescription($request->input('description'));
        $newProduct->setPrice($request->input('price'));
        $newProduct->setPath("game.png");
        $newProduct->save();
        if ($request->hasFile('path')) {
            $imageName = $newProduct->getId().".".$request->file('path')->extension();
            Storage::disk('public')->put(
                $imageName,
                file_get_contents($request->file('path')->getRealPath())
            );
            $newProduct->setPath($imageName);
            $newProduct->save();
        }

        return back();
    }

    public function delete($id)
    {
        Product::destroy($id);
        return back();
    }

    public function edit($id)
    {

        $viewData = [];
        $viewData["title"] = "Admin Page - Edit Product - Kiosk Mang Alan";
        $viewData["product"] = Product::findOrFail($id);
        return view('admin.product.edit')->with("viewData", $viewData);
    }

    public function update(Request $request, $id)
    {
        Product::validate($request);
        $product = Product::findOrFail($id);
        $product->setName($request->input('name'));
        $product->setDescription($request->input('description'));
        $product->setPrice($request->input('price'));
        if ($request->hasFile('path')) {
            $imageName = $product->getId().".".$request->file('path')->extension();
            Storage::disk('public')->put(
                $imageName,
                file_get_contents($request->file('path')->getRealPath())
            );
            $product->setPath($imageName);
        }
        $product->save();
        return redirect()->route('admin.product.index');
    }

}

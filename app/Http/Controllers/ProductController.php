<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Order;
use App\Models\Item;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{

    // public static $products = [
    //     ["id"=>"1", "name"=>"TV", "description"=>"Best TV", "image" => "game.png", "price"=>"1000"],
    //     ["id"=>"2", "name"=>"iPhone", "description"=>"Best iPhone", "image" => "safe.png", "price"=>"999"],
    //     ["id"=>"3", "name"=>"Chromecast", "description"=>"Best Chromecast", "image" => "submarine.png", "price"=>"30"],
    //     ["id"=>"4", "name"=>"Glasses", "description"=>"Best Glasses", "image" => "game.png", "price"=>"100"]
    //     ];

    public function index()
    {

        $viewData = [];
        $viewData["title"] = "Menu - Kiok Mang Alan";
        $viewData["subtitle"] = "Daftar Menu";
        $viewData["products"] = Product::all();

        return view('product.index')->with("viewData", $viewData);
    }

    public function show($id)
    {
        $viewData = [];
        $product = Product::findOrFail($id);
        $viewData["title"] = $product->getName()."- Kiok Mang Alan";
        $viewData["subtitle"] = $product->getName()." -  Detail Produk";
        $viewData["products"] = $product;

        return view('product.show')->with("viewData", $viewData);
    }
}

@extends('layouts.app')
@section('title', $viewData["title"])
@section('subtitle', 'Pembayaran')
@section('content')
<div class="card">
    <div class="card-header">
        Pembayaran Berhasil :)
    </div>
    <div class="card-body">
        <div class="alert alert-success" role="alert">
            Horee, pembayaran telah berhasil. Nomor transaksi anda adalah <b>#{{ $viewData["order"]->getId() }}</b>
        </div>
    </div>
</div>
@endsection

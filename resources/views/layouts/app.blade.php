<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <title>@yield('title', 'Kiosk Mang Alan')</title>
    </head>
    <body>
        <!-- header -->
        <nav class="navbar navbar-expand-lg navbar-dark bg-secondary py-4">
            <div class="container">
                <a class="navbar-brand" href="{{route('home.index')}}">Kiosk Mang Alan</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav ms-auto">
                        <a class="nav-link active" href="{{route('home.index')}}">Beranda</a>
                        <a class="nav-link active" href="{{ route('product.index') }}">Menu</a>
                        <a class="nav-link active" href="{{ route('cart.index') }}"><i class="bi bi-bag-plus-fill"></i></a>
                        <a class="nav-link active" href="{{route('home.about')}}"><i class="bi bi-chat"></i></a>
                        <div class="vr bg-white mx-2 d-none d-lg-block"></div>
                            @guest
                                <a class="nav-link active" href="{{ route('login') }}">Masuk</a>
                                <a class="nav-link active" href="{{ route('register') }}">Daftar</a>
                            @else
                                <a class="nav-link active" href="{{ route('myaccount.orders') }}"><i class="bi bi-cart"></i></a>
                                <form id="logout" action="{{ route('logout') }}" method="POST">
                                    <a role="button" class="nav-link active" onclick="document.getElementById('logout').submit();">Keluar</a>
                                    @csrf
                                </form>
                            @endguest
                    </div>
                </div>
            </div>
        </nav>
        <header class="masthead bg-danger text-white text-center py-4">
            <div class="container d-flex align-items-center flex-column">
                <h2>@yield('subtitle', 'Boengsoe Caffe')</h2>
            </div>
        </header>
        <!-- header -->
        <div class="container my-4">
            @yield('content')
        </div>
        <!-- footer -->
        <div class="copyright bg-secondary py-4 text-center text-white">
            <div class="container">
                <small>
                Copyright - <a class="text-reset fw-bold text-decoration-none" target="_blank" href="https://twitter.com/addsyarif">
                Adrisa Syarif
                </a> @<b>2022</b>
                </small>
            </div>
        </div>
        <!-- footer -->

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous">
        </script>
    </body>
</html>

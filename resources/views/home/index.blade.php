@extends('layouts.app')
@section('title', $viewData["title"])
@section('content')
    <div class="row">
        <div class="col-md-6 col-lg-4 mb-2">
            <img width="420" src="{{ asset('/img/cilok.jpeg') }}" class="img-fluid rounded">
        </div>
        <div class="col-md-6 col-lg-4 mb-2">
            <img width="400" src="{{ asset('/img/warung.jpg') }}" class="img-fluid rounded">
        </div>
        <div class="col-md-6 col-lg-4 mb-2">
            <img width="400" src="{{ asset('/img/kelapa-muda.png') }}" class="img-fluid rounded">
        </div>
    </div>
@endsection
